import React, { useRef, useState, useEffect } from "react";
import { Form, Card, Button, Alert, Tab, Tabs, Table } from "react-bootstrap";
import { useAuth } from "../contexts/AuthContext";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import Loader from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

export default function Dashboard() {
  const [error, setError] = useState("");
  const [log, setLogs] = useState([]);
  const history = useHistory();
  const emailRef = useRef();
  const passwordRef = useRef();
  const passwordConfirmRef = useRef();
  const passwordRefCofre = useRef();
  const passwordConfirmRefCofre = useRef();
  const { currentUser, updatePassword, updateEmail, logout, setPasswordCofre } =
    useAuth();
  const [loading, setLoading] = useState(false);
  const [loaderFind, setLoaderFind] = useState(false);

  function handleSubmit(e) {
    e.preventDefault();
    if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      return setError("Passwords do not match");
    }

    const promises = [];
    setLoading(true);
    setError("");

    if (emailRef.current.value !== currentUser.email) {
      promises.push(updateEmail(emailRef.current.value));
    }
    if (passwordRef.current.value) {
      promises.push(updatePassword(passwordRef.current.value));
    }

    Promise.all(promises)
      .then(() => {
        history.push("/");
      })
      .catch(() => {
        setError("Failed to update account");
      })
      .finally(() => {
        setLoading(false);
      });
  }
  async function handleLogout() {
    setError("");
    try {
      await logout();
      history.push("/login");
    } catch {
      setError("Failed to log out");
    }
  }
  async function handleSubmitCofre(e) {
    e.preventDefault();
    if (
      passwordRefCofre.current.value !== passwordConfirmRefCofre.current.value
    ) {
      return setError("Passwords do not match");
    }
    try {
      setLoading(true);
      await setPasswordCofre(passwordRefCofre.current.value);
      window.location.reload();
    } catch {}
    setLoading(false);
  }

  useEffect(() => {
    setLoaderFind(true);
    const api = axios.create({
      baseURL:
        "https://firestore.googleapis.com/v1/projects/cofreinteligente-8f272/databases/(default)/documents/conection",
    });
    api.get("/").then((response) => {
      setLogs(response.data.documents);
    });
    setTimeout(function () {
      setLoaderFind(false);
    }, 3000);
  }, []);

  if (!!loaderFind) {
    return (
      <>
        <Loader
          type="ThreeDots"
          color="red"
          height={100}
          width={100}
          style={{ display: "flex", justifyContent: "center" }}
        />
      </>
    );
  } else {
    return (
      <>
        <Tabs
          defaultActiveKey="profile"
          id="uncontrolled-tab-example"
          className="mb-3"
        >
          <Tab eventKey="profile" title="Logs referente ao cofre">
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Data</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                {!!log &&
                  log.map((logMap, index) => (
                    <tr key={index}>
                      {console.log(logMap)} <td>{index}</td>
                      <td>{String(logMap.fields.data.stringValue)}</td>
                      <td>{String(logMap.fields.resposta.booleanValue)}</td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          </Tab>
          <Tab eventKey="home" title="Alterar senha do cofre">
            <Card>
              <Card.Body>
                <h2 className="text-center mb-4">Cofre</h2>
                {error && <Alert variant="danger">{error}</Alert>}
                <Form onSubmit={handleSubmitCofre}>
                  <Form.Group id="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      type="number"
                      ref={passwordRefCofre}
                      placeholder="Deixe em branco para manter o mesmo"
                    />
                  </Form.Group>
                  <Form.Group id="password-confirm">
                    <Form.Label>Password Confirmation</Form.Label>
                    <Form.Control
                      type="number"
                      ref={passwordConfirmRefCofre}
                      placeholder="Deixe em branco para manter o mesmo"
                    />
                  </Form.Group>
                  <Button disabled={loading} className="w-100" type="submit">
                    Atualizar
                  </Button>
                </Form>
              </Card.Body>
            </Card>
          </Tab>
          <Tab eventKey="contact" title="Alterar senha de login">
            <Card>
              <Card.Body>
                <h2 className="text-center mb-4">Atualizar Perfil</h2>
                {error && <Alert variant="danger">{error}</Alert>}
                <Form onSubmit={handleSubmit}>
                  <Form.Group id="email">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                      type="email"
                      ref={emailRef}
                      required
                      defaultValue={currentUser.email}
                    />
                  </Form.Group>
                  <Form.Group id="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      type="password"
                      ref={passwordRef}
                      placeholder="Deixe em branco para manter o mesmo"
                    />
                  </Form.Group>
                  <Form.Group id="password-confirm">
                    <Form.Label>Password Confirmation</Form.Label>
                    <Form.Control
                      type="password"
                      ref={passwordConfirmRef}
                      placeholder="Deixe em branco para manter o mesmo"
                    />
                  </Form.Group>
                  <Button disabled={loading} className="w-100" type="submit">
                    Atualizar
                  </Button>
                </Form>
              </Card.Body>
            </Card>
            <div className="w-100 text-center mt-2">
              <Link to="/">Cancelar</Link>
            </div>
          </Tab>
        </Tabs>
        <div className="w-100 text-center mt-2">
          <Button variant="link" onClick={handleLogout}>
            Sair
          </Button>
        </div>
      </>
    );
  }
}
