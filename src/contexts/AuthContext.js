import axios from "axios";
import { format } from "date-fns";
import "firebase/firestore";
import React, { useContext, useEffect, useState } from "react";
import firebase, { auth } from "../firebase";

const AuthContext = React.createContext();
const firestore = firebase.firestore();

export function useAuth() {
  return useContext(AuthContext);
}

export function AuthProvider({ children }) {
  const [currentUser, setCurrentUser] = useState();
  const [loading, setLoading] = useState(true);

  function signup(email, password) {
    return auth.createUserWithEmailAndPassword(email, password);
  }

  function login(email, password) {
    return auth.signInWithEmailAndPassword(email, password);
  }

  function logout() {
    return auth.signOut();
  }

  function resetPassword(email) {
    return auth.sendPasswordResetEmail(email);
  }

  function updateEmail(email) {
    return currentUser.updateEmail(email);
  }

  function updatePassword(password) {
    return currentUser.updatePassword(password);
  }

  async function setPasswordCofre(senhaCofre) {
    const senha = String(senhaCofre);
    await axios.patch(
      "https://cofreinteligente-8f272-default-rtdb.firebaseio.com/Cofre.json",
      { senha: senha }
    );
    return firestore.collection("senha").doc("YJwmYDpY7wFn4ShHi4rW").set({
      password: senha,
    });
  }

  async function tryConection(senhaCofre) {
    const senha = String(senhaCofre);
    const data = new Date();
    const data_nova = format(data, "dd/MM/yyyy");
    axios.get()
    var docRef = await firestore
      .collection("senha")
      .doc("YJwmYDpY7wFn4ShHi4rW");
    docRef
      .get()
      .then((doc) => {
        if (doc.exists) {
          // console.log("Document data:", doc.data());
          if (doc.data().password === senha) {
            axios.patch(
              "https://cofreinteligente-8f272-default-rtdb.firebaseio.com/Cofre.json",
              { conection: true }
            );
            firestore.collection("conection").doc().set({
              resposta: true,
              data: data_nova,
            });
          } else {
            axios.patch(
              "https://cofreinteligente-8f272-default-rtdb.firebaseio.com/Cofre.json",
              { conection: false }
            );
            firestore.collection("conection").doc().set({
              resposta: false,
              data: data_nova,
            });
          }
        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
      })
      .catch((error) => {
        console.log("Error getting document:", error);
      });
  }

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((user) => {
      setCurrentUser(user);
      setLoading(false);
    });

    return unsubscribe;
  }, []);

  const value = {
    currentUser,
    login,
    signup,
    logout,
    resetPassword,
    updateEmail,
    updatePassword,
    setPasswordCofre,
    tryConection,
  };

  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  );
}
