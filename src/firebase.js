import firebase from "firebase/app"
import "firebase/auth"

const app = firebase.initializeApp({
  apiKey: "AIzaSyAxfbTwklILbmRh4ISmuW9JeYHM1Fwi608",
  authDomain: "cofreinteligente-8f272.firebaseapp.com",
  projectId: "cofreinteligente-8f272",
  databaseURL: "https://cofreinteligente-8f272.firebaseio.com",
  storageBucket: "cofreinteligente-8f272.appspot.com",
  messagingSenderId: "909272467273",
  appId: "1:909272467273:web:bb789ce3ab6e5626a8fc98",
  measurementId: "G-ZS7MZJYF30"
})

export const auth = app.auth()
export default app
