import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import { Alert, Button, Card, CardGroup, Form } from "react-bootstrap";
import Loader from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { Link, useHistory } from "react-router-dom";
import { useAuth } from "../contexts/AuthContext";

export default function Login() {
  const emailRef = useRef();
  const passwordRef = useRef();
  const { login, tryConection } = useAuth();
  const [error, setError] = useState("");
  const [fail, setFail] = useState("");
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  const [valuepass, setValuepass] = useState("");
  const [loaderFind, setLoaderFind] = useState(false);
  const [password, setPassword] = useState("");

  useEffect(() => {
    setLoaderFind(true);
    const api = axios.create({
      baseURL:
        "https://firestore.googleapis.com/v1/projects/cofreinteligente-8f272/databases/(default)/documents/senha",
    });
    api.get("/").then((response) => {
      setPassword(response.data.documents[0].fields.password.stringValue);
    });
    setTimeout(function () {
      setLoaderFind(false);
    }, 3000);
  }, []);

  async function handleSubmit(e) {
    e.preventDefault()

    try {
      setError("")
      setLoading(true)
      await login(emailRef.current.value, passwordRef.current.value)
      history.push("/admin")
    } catch {
      setError("Failed to log in")
    }
    setLoading(false)
  }

  async function handleConection(e) {
    await axios
      .get(
        "https://cofreinteligente-8f272-default-rtdb.firebaseio.com/Cofre.json"
      )
      .then((response) => {
        console.log(response.data.porta);
        if (response.data.porta === true) {
          setFail("Porta está aberta, login não permetido");
          return
        } else {
          try {
            setFail("");
            setLoading(true);
            tryConection(valuepass);
          } catch {
            setFail("Conexão não estabelecida");
          }
          console.log(password);
          if (password === valuepass) {
            setFail("Login realizado com sucesso");
          } else {
            setFail("Login negado");
          }
        }
      });
    setLoading(false);
  }

  function closePortao() {
    axios.patch(
      "https://cofreinteligente-8f272-default-rtdb.firebaseio.com/Cofre.json",
      { conection: false, porta: false }
    );
  }

  if (!!loaderFind) {
    return (
      <>
        <Loader
          type="ThreeDots"
          color="red"
          height={100}
          width={100}
          style={{ display: "flex", justifyContent: "center" }}
        />
      </>
    );
  } else {
    return (
      <>
        <CardGroup style={{ justifyContent: "center" }}>
          <Card
            style={{
              maxWidth: "250px",
              marginRight: "50px",
              border: "1px solid rgba(0,0,0,.125)",
            }}
          >
            <Card.Body>
              <h2 className="text-center mb-4">Log In</h2>
              {error && <Alert variant="danger">{error}</Alert>}
              <Form onSubmit={handleSubmit}>
                <Form.Group id="email">
                  <Form.Label>Email</Form.Label>
                  <Form.Control type="email" ref={emailRef} required />
                </Form.Group>
                <Form.Group id="password">
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" ref={passwordRef} required />
                </Form.Group>
                <Button disabled={loading} className="w-100" type="submit">
                  Log In
                </Button>
              </Form>
              <div className="w-100 text-center mt-3">
                <Link to="/forgot-password">Esqueceu sua senha?</Link>
              </div>
              <div className="w-100 text-center mt-2">
                Precisa de uma conta? <Link to="/signup">Sign Up</Link>
              </div>
            </Card.Body>
          </Card>
          <Card
            style={{ maxWidth: "450px", border: "1px solid rgba(0,0,0,.125)" }}
          >
            <div
              className="btn-group-vertical ml-4 mt-4"
              role="group"
              style={{ maxWidth: "400px", margin: "auto!important" }}
            >
              <div className="btn-group">
                <input
                  className="text-center form-control-lg mb-4"
                  style={{ width: "100%" }}
                  id="code"
                  value={valuepass}
                />
              </div>
              <div className="btn-group">
                <button
                  type="button"
                  className="btn   py-3"
                  onClick={() => setValuepass(`${valuepass}${"1"}`)}
                >
                  1
                </button>
                <button
                  type="button"
                  className="btn   py-3"
                  onClick={() => setValuepass(`${valuepass}${"2"}`)}
                >
                  2
                </button>
                <button
                  type="button"
                  className="btn   py-3"
                  onClick={() => setValuepass(`${valuepass}${"3"}`)}
                >
                  3
                </button>
              </div>
              <div className="btn-group">
                <button
                  type="button"
                  className="btn   py-3"
                  onClick={() => setValuepass(`${valuepass}${"4"}`)}
                >
                  4
                </button>
                <button
                  type="button"
                  className="btn   py-3"
                  onClick={() => setValuepass(`${valuepass}${"5"}`)}
                >
                  5
                </button>
                <button
                  type="button"
                  className="btn   py-3"
                  onClick={() => setValuepass(`${valuepass}${"6"}`)}
                >
                  6
                </button>
              </div>
              <div className="btn-group">
                <button
                  type="button"
                  className="btn   py-3"
                  onClick={() => setValuepass(`${valuepass}${"7"}`)}
                >
                  7
                </button>
                <button
                  type="button"
                  className="btn   py-3"
                  onClick={() => setValuepass(`${valuepass}${"8"}`)}
                >
                  8
                </button>
                <button
                  type="button"
                  className="btn   py-3"
                  onClick={() => setValuepass(`${valuepass}${"0"}`)}
                >
                  9
                </button>
              </div>
              <div className="btn-group">
                <button
                  type="button"
                  className="btn   py-3"
                  onClick={() => setValuepass("")}
                >
                  &lt;
                </button>
                <button
                  type="button"
                  className="btn   py-3"
                  onClick={() => setValuepass(`${valuepass}${"0"}`)}
                >
                  0
                </button>
                <button
                  type="button"
                  className="btn btn-primary py-3"
                  onClick={() => handleConection()}
                >
                  Go
                </button>
              </div>
              {fail ? (
                <Alert variant="info">{fail}</Alert>
              ) : (
                <Alert variant="danger">{fail}</Alert>
              )}
            </div>
          </Card>
        </CardGroup>
        <button
          type="button"
          className="w-10 btn btn-primary"
          style={{ "margin": "15px auto", "display": "flex" }}
          onClick={() => closePortao()}
        >
          Fechar
        </button>
      </>
    );
  }
}
